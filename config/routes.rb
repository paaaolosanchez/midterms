Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  get 'home/bio', to: 'home#bio'
  # get 'cars/new', to: 'cars#new'
  get 'cars/all', to: 'cars#all'
  get '/signup', to: 'users#new'
  resources :cars
  resources :users, except: [:new]
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
end
