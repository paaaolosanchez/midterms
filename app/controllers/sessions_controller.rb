class SessionsController < ApplicationController
  def new
    render 'new'
  end

  def create
    # debugger
    user = User.find_by(email: params[:session][:email].downcase)

    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      flash[:success] = 'logged in'
      redirect_to user_path(user)
    else
      flash.now[:danger] = "tanga amputa"

      render 'new'
    end

  end

  def destroy
    session[:user_id] = nil
    flash[:notice] = "Logged out"
    redirect_to root_path
  end
end
