class UsersController < ApplicationController
  def new
    @user = User.new
  end
  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = "New User"
      redirect_to user_path(@user)
    else
      render 'new'
    end
  end
  def show
    @user = User.find(params[:id])
  end
  def index
    @user = User
  end
  def edit
    @user = User.find(params[:id])
  end
  def update
    @user = User.find(params[:id])
    @user = User.update(user_params)
    flash[:notice] = 'nice'
    redirect_to users_path(@user)
  end
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = 'cool'
    redirect_to users_path
  end
  private
    def user_params
      params.require(:user).permit(:username, :email, :password)
    end
end
