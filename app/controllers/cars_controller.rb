class CarsController < ApplicationController
  def new
    @car = Car.new
  end
  def create
  @car = Car.new(car_params)
  @car.user = current_user
  if @car.save
    flash[:notice] = "New Car Parked"
    redirect_to car_path(@car)
  else
    render 'new'
  end
end

def show
  @car = Car.find(params[:id])
end

def index
  @car = Car
end

def edit
  @car = Car.find(params[:id])
end

def update
  @car = Car.find(params[:id])
  @car = Car.update(car_params)
  flash[:notice] = 'nice'
  redirect_to car_path(@car)
end

def destroy
  @car = Car.find(params[:id])
  @car.destroy
  flash[:notice] = 'cool'
  redirect_to cars_path
end


private
  def car_params
    params.require(:car).permit(:plate_number, :time_in, :time_out, :color, :brand)
  end
end
