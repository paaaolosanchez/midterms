class Car < ActiveRecord::Base
  belongs_to :user
   validates :plate_number, presence:true, length: { minimum: 4, maximum: 8 }
   validates :time_in, presence:true
   validates :time_out, presence:true
end
