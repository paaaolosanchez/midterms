class AddPlateNumber < ActiveRecord::Migration[5.2]
  def change
    add_column :cars, :plate_number, :string
  end
end
