class AddDetailsToCars < ActiveRecord::Migration[5.2]
  def change
      	add_column :cars, :color, :string
        add_column :cars, :brand, :string
        add_column :cars, :time_in, :datetime
        add_column :cars, :time_out, :datetime
  end
end
